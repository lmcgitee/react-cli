module.exports = {
  // 使用react官方预设规则 需要安装 babel-preset-react-app
  presets: [['react-app', { flow: false, typescript: true }]],
};
