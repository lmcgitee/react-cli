// eg: https://github.com/styled-components/styled-components/issues/3607
module.exports = {
  processors: ['stylelint-processor-styled-components'],
  extends: [
    'stylelint-config-standard-scss',
    'stylelint-config-styled-components',
    'stylelint-prettier/recommended',
  ],
  customSyntax: 'postcss-scss',
  rules: {
    'no-descending-specificity': [
      true,
      {
        severity: 'warning',
      },
    ],
  },
};
