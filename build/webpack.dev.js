const path = require('path');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const baseConfig = require('./webpack.base.js');
const { merge } = require('webpack-merge');

module.exports = merge(baseConfig, {
  mode: 'development',
  plugins: [
    new ReactRefreshWebpackPlugin(), // 热更新 解决全局变量的问题
  ],
  devServer: {
    open: true, // 自动打开浏览器
    port: 8080,
    host: 'localhost',
    hot: true, //开启热更新
    compress: false, // gzip压缩 开发环境关闭可以提高热更新速度
    historyApiFallback: true, // 解决history路由404问题
    static: path.join(__dirname, '../public'),
  },
  devtool: 'eval-cheap-module-source-map', // 代码映射
});
