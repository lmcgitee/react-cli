const prodConfig = require('./webpack.prod.js');
const devConfig = require('./webpack.dev.js');
const SpeedMeasureWebpackPlugin = require('speed-measure-webpack-plugin'); // 打包速度分析插件
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer'); // 分析打包结果插件
const { merge } = require('webpack-merge');
const smp = new SpeedMeasureWebpackPlugin();

const isProd = process.env.NODE_ENV === 'production';

const config = isProd ? prodConfig : devConfig;

// 用来分析打包速度
module.exports = smp.wrap(
  merge(config, {
    plugins: [new BundleAnalyzerPlugin()], // 配置分析打包结果插件
  })
);
