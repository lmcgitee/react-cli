const path = require('path');
const ESlintWebpackPlugin = require('eslint-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isProd = process.env.NODE_ENV === 'production';

const getStyleLoaders = (loader) => {
  return [
    // 生产模式下使用plugin提取css成单独文件
    isProd ? MiniCssExtractPlugin.loader : 'style-loader', // 动态创建style标签，将模块化的css引入
    'css-loader', // 将css文件编译成webpack能识别的模块
    {
      loader: 'postcss-loader', // 处理样式兼容性问题
      options: {
        postcssOptions: {
          plugins: ['postcss-preset-env'], // 预设值 可以解决大多数样式兼容性问题
        },
      },
    },
    loader,
  ].filter(Boolean); // 过滤undefined
};

module.exports = {
  entry: './src/index.tsx', // 入口文件的指定 绝对路径和相对路径均可
  output: {
    path: path.resolve(__dirname, '../dist'), // 开发模式不需要输出可以设置为undefined
    filename: 'static/js/[name].[contenthash:8].js',
    clean: true, // webpack5之前的版本需要使用clean-webpack-plugin来实现打包的时候清空上一次结果， webpack5内置了
  },
  module: {
    rules: [
      {
        oneOf: [
          {
            test: /\.css$/, // 匹配css结尾的文件
            include: [path.resolve(__dirname, '../src')], //只处理src下的文件
            use: getStyleLoaders(),
          },
          {
            test: /\.less$/, // 匹配less结尾的文件
            include: [path.resolve(__dirname, '../src')], //只处理src下的文件
            use: getStyleLoaders('less-loader'),
          },
          {
            test: /\.(tsx|ts)$/,
            include: path.resolve(__dirname, '../src'),
            loader: 'babel-loader',
            options: {
              cacheDirectory: true, // 缓存babel-loader的转换结果
              cacheCompression: false, // 缓存结果不压缩
              plugins: [
                !isProd && 'react-refresh/babel', // 开启js的HMR功能
              ].filter(Boolean),
            },
          },
          {
            test: /\.(png|jpe?g|gif|svg|)$/, // 匹配图片文件
            type: 'asset', // webpack5内置了文件资源处理的配置
            parser: {
              dataUrlCondition: {
                maxSize: 10 * 1024, // 小于10kb的图片会转为base64 转base64之后图片体积会变大，因此设置10kb的阀值
              },
            },
            generator: {
              filename: 'static/images/[name].[contenthash:6][ext]', // 哈希值取6位,保留原有后缀
            },
          },
          {
            test: /\.(woff2?|eot|ttf|otf|)$/, // 匹配字体图标文件
            type: 'asset',
            parser: {
              dataUrlCondition: {
                maxSize: 10 * 1024,
              },
            },
            generator: {
              filename: 'static/fonts/[name].[contenthash:6][ext]',
            },
          },
          {
            test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)$/, // 匹配媒体文件
            type: 'asset',
            generator: {
              filename: 'static/media/[name].[contenthash:6][ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    // 实例化eslint插件
    new ESlintWebpackPlugin({
      context: path.resolve(__dirname, '../src/'), // 只对src下面的文件做校验
      exclude: 'node_modules', // 排除node_modules 默认值
      cache: true, //开启eslint的校验结果缓存
      cacheLocation: path.resolve(
        __dirname,
        '../node_modules/.cache/.eslintCache'
      ), //指定缓存的位置
    }),
    new HtmlWebpackPlugin({
      // 以public下的html文件为模版创建新的html文件
      // 内容与原来的index.html一致并且会自动引入打包后的js
      template: path.resolve(__dirname, '../public/index.html'),
    }),
  ],
  // webpack解析模块时加载的选项
  resolve: {
    extensions: ['.ts', '.tsx'], // 自动补全文件扩展名
    alias: {
      '@': path.resolve(__dirname, '../src'),
    },
    modules: [path.resolve(__dirname, '../node_modules')],
  },
  // 开启webpack持久化存储缓存
  cache: {
    type: 'filesystem', // 使用文件缓存
  },
};
