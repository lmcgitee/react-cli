module.exports = {
  // 校验eslint和prettier错误 并自动修复 stylelint最新版本不支持自动修复
  'src/**/*.{ts,tsx}': ['eslint --fix', 'prettier --write', 'npm run lint:css'],
  'src/**/*.ts?(x)': () => ['tsc -p ./tsconfig.json --noEmit'],
  'src/**/*.{html,css,less,md,json}': ['prettier --write'],
};
