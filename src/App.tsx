import React from 'react';
import styled from 'styled-components';
const Wrapper = styled.div<{ visible: boolean }>`
  color: red;
`;

const App = () => {
  // eslint-disable-next-line no-console
  console.log(1234);

  console.error('这里是一个console规则测试');
  return (
    <div>
      这里是一个react App组件
      <Wrapper visible>这里是一个styled-components的ui组件</Wrapper>
    </div>
  );
};

export const AppTwo = () => {
  const arr = [1, 2, 3];
  return (
    <>
      {arr.map((item, index) => (
        <div key={index}>1234</div>
      ))}
    </>
  );
};

export default App;
