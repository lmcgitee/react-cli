module.exports = {
  parserOptions: {
    babelOptions: {
      // 修复babel-preset-react-app的报错
      presets: [
        ['babel-preset-react-app', false],
        'babel-preset-react-app/prod',
      ],
    },
  },
  extends: [
    'react-app', // 继承 react 官方规则
    'plugin:prettier/recommended', // prettier规则
  ],
  rules: {
    'prettier/prettier': 'error',
    'no-console': ['error', { allow: ['warn', 'error'] }],
    'import/no-named-as-default': ['error'],
    '@typescript-eslint/no-explicit-any': ['error'],
    '@typescript-eslint/no-unused-vars': ['error'],
    'react/jsx-key': ['error'],
  },
};
