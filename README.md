# react-cli

#### 介绍

使用 react18 + webpack5 + ts + antd 搭建的 react-cli 配置项还在持续集成中...

#### 软件架构

#### 安装教程

1. yarn install
2. yarn dev

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 注意事项

1. 项目内的 stylelint 使用了 v14 版本，这个版本针对 css-in-js 语法无法使用 --fix 只能检测出来错误手动修复
   可以参照 styled-components 这个 github repo 内的 issue:https://github.com/styled-components/styled-components/issues/3607,
   后续有其他更好的可行性方案之后，会再补充配置
2. eslint 不会主动检查 ts 类型 需要使用 tsc 自行检测
